import os
import re
import html
import sys
import unicodedata

# Specify directories
train_postive  = 'aclImdb/train/pos'
train_negative = 'aclImdb/train/neg'
test_postive   = 'aclImdb/test/pos'
test_negative  = 'aclImdb/test/neg'

# Output file names
train_out = 'train.csv'
test_out  = 'test.csv'


def strip_accents(val):
    """
    Replaces accented characters from a string with their regular counterparts.
    
    Args:
        val         String with accented characters
        
    Returns:
        String with accented characters replaced
    """
    # Check for string type
    if not (type(val) == str or type(val) == unicode):
        return val
    
    # Return string without accented characters
    return ''.join(c for c in unicodedata.normalize('NFD', val) if unicodedata.category(c) != 'Mn')

    
def clean_review(text):
    """
    Performs several cleaning operations to the text of a review.
    
    Args:
        text        String containing the review's text
        
    Returns:
        String containing the cleansed text of the review
    """
    # Convert to lower case
    text = text.lower()
                        
    # Strip accented characters
    text = strip_accents(text)

    # Remove HTML (quick 'n dirty)
    text = html.unescape(text)
    text = re.sub('<[^<]+?>', ' ', text)
    
    # Remove punctuation
    text = re.sub('["\?!\.,:;\-_\*&#()<>/]', ' ', text)
    
    # Strip spaces
    text = re.sub('\s+', ' ', text)
    text = text.strip()

    # Return cleansed text
    return text
    
    
def process_dirs(dirs, out_file):
    """
    Processes directories with review text files. Reads each review file, cleans the text then combines them into a single CSV file.
    
    Args:
        dirs        Dictionary of {label: directory} pairs
        out_file    Output filename    
    """
    # Try opening the output file
    with open(out_file, 'w', encoding='utf8') as out_fh:
    
        # Write headers
        out_fh.write('"Label";"Text"\n')
    
        # Loop over input directories
        for label, dir in dirs.items():
        
            # Check whether the input directory exists
            if not os.path.isdir(dir):
                raise Exception('Directory "%s" does not exist!' % dir)
            
            # Loop over files
            files = os.listdir(dir)
            for count, fname in enumerate(files):
                # Print progression
                perc = (count / len(files)) * 100
                sys.stdout.write('\rProcessing directory: %25s --- %3.0f%%' % (dir, perc))
                sys.stdout.flush()
                
                # Construct path to file
                full = os.path.join(dir, fname)
                
                # Try reading the file
                with open(full, 'r', encoding='utf8') as in_fh:
                    # Read text
                    text = in_fh.read()
                    
                    # Cleanse the text
                    text = clean_review(text)
                    
                    # Add label and review text to the output file
                    out_fh.write('"%s";"%s"\n' % (label, text))


def main():
    """
    Main program logic
    """
    # Create train dataset
    train_dirs = {
        'pos': train_postive,
        'neg': train_negative,
    }
    process_dirs(train_dirs, train_out)
    
    # Create test dataset
    test_dirs = {
        'pos': test_postive,
        'neg': test_negative,
    }
    process_dirs(test_dirs, test_out)
    
            
# Start main
if __name__ == '__main__':
    main()