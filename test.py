import csv
import time
import pickle

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, precision_score, recall_score

# Test data input file
data_file = 'test.csv'

# Model input files
vectorizer_file    = 'tfidf.pickle'
logregression_file = 'logreg.pickle'

# Predictions output file
result_file = 'results.csv'


def main():
    """
    Main program logic. Loads trained vectorizer and classifier and applies it to the test dataset. Prints accuracy metrics for the model.
    """
    # Set start time
    start_time = time.time()
    print('%5.2f seconds --- Starting...' % (time.time() - start_time))
    
    # Load the vectorizer and classifier
    print('%5.2f seconds --- Loading models...' % (time.time() - start_time))
    with open(vectorizer_file, 'rb') as vect_fh:
        vect = pickle.load(vect_fh)
    with open(logregression_file, 'rb') as clf_fh:
        clf = pickle.load(clf_fh)
    
    # Read test labels and texts
    print('%5.2f seconds --- Reading test data...' % (time.time() - start_time))
    labels = []
    texts  = []
    with open(data_file, 'r', encoding='utf8') as in_fh:
        # Create CSV reader
        in_csv = csv.DictReader(in_fh, delimiter=';')

        # Process the CSV file (format "Label";"Text")
        for review in in_csv:
            # Store label and text separately
            labels.append(review['Label'])
            texts.append(review['Text'])
    
    # Vectorize the text creating a Document Term Matrix
    print('%5.2f seconds --- Creating DTM...' % (time.time() - start_time))
    dtm = vect.transform(texts)
    
    # Predict on test set
    print('%5.2f seconds --- Scoring test data...' % (time.time() - start_time))
    pred_classes = clf.predict(dtm)
    pred_probabs = clf.predict_proba(dtm)
    
    # Write results to file
    print('%5.2f seconds --- Writing results to file...' % (time.time() - start_time))
    with open(result_file, 'w', encoding='utf8') as res_fh:
        # Write headers
        res_fh.write('"Label";"Predicted";"Probability";"Correct";"Text"\n')
        
        # Write labels, predictions, accuracy and texts to file
        for label, pclass, pprob, text in zip(labels, pred_classes, pred_probabs, texts):
            # Correctly classified (1) or not (0)
            pcorrect = 1 if label == pclass else 0
            
            # Probability of the predicted class
            pprob = pprob[clf.classes_.tolist().index(pclass)]
            
            # Write record
            res_fh.write('"%s";"%s";"%0.2f";"%d";"%s"\n' % (label, pclass, pprob, pcorrect, text))
    
    # Done testing    
    print('%5.2f seconds --- All done!' % (time.time() - start_time))

    # Print results
    print('-' * 70)
    print('Results:')
    print('-' * 70)
    
    print('%-15s %-50s %3d%%' % ('Accuracy', '(correctly classified / total reviews)', accuracy_score(labels, pred_classes) * 100))
    print('%-15s %-50s %3d%%' % ('Precision', '(true positives / predicted positives)', precision_score(labels, pred_classes, pos_label='pos') * 100))
    print('%-15s %-50s %3d%%' % ('Recall', '(true positives / total positives)', recall_score(labels, pred_classes, pos_label='pos') * 100))
    

# Start main
if __name__ == '__main__':
    main()