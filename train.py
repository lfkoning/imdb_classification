import csv
import time
import pickle

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression


# Train data input file
data_file = 'train.csv'

# Model output files
vectorizer_file    = 'tfidf.pickle'
logregression_file = 'logreg.pickle'


def main():
    """
    Main program logic. Trains a vectorizer and logistic regression classifier, keep track of run time.
    """
    # Set start time
    start_time = time.time()
    print('%5.2f seconds --- Starting...' % (time.time() - start_time))
        
    # Read train labels and texts
    print('%5.2f seconds --- Reading train data...' % (time.time() - start_time))
    labels = []
    texts  = []
    with open(data_file, 'r', encoding='utf8') as in_fh:
        # Create CSV reader
        in_csv = csv.DictReader(in_fh, delimiter=';')

        # Process the CSV file (format "Label";"Text")
        for review in in_csv:
            # Store label and text separately
            labels.append(review['Label'])
            texts.append(review['Text'])
            
    # Create TF-IDF vectorizer
    print('%5.2f seconds --- Creating DTM...' % (time.time() - start_time))
    vect = TfidfVectorizer(
        stop_words = 'english',     # Ignore stopwords
        max_df = 0.7,               # Max frequency: present in no more than 70 percent of the reviews
        min_df = 10,                # Min frequency: present in at least 10 reviews
    )
    
    # Train the vectorizer and transform the texts with it
    # Result will be a document term matrix with (weighted) word frequencies for each review
    dtm = vect.fit_transform(texts)

    # Train logistic regression classifier
    # Train a classifier using the word frequencies as predictors and the labels as dependent variable
    print('%5.2f seconds --- Training classifier...' % (time.time() - start_time))
    clf = LogisticRegression()
    clf.fit(dtm, labels)
    
    # Store vectorizer and classifier
    # Write the models to file using the pickle format (serialized object notation)
    print('%5.2f seconds --- Writing models...' % (time.time() - start_time))
    with open(vectorizer_file, 'wb') as vect_fh:
        pickle.dump(vect, vect_fh)
    with open(logregression_file, 'wb') as clf_fh:
        pickle.dump(clf, clf_fh)
    
    # Finished!
    print('%5.2f seconds --- All done!' % (time.time() - start_time))
 

# Start main 
if __name__ == '__main__':
    main()