# IMDB Review classification


## Overview

The goal of this project is to classify IMDB reviews as either positive or negative. The project is meant to be a tutorial for text classification which is why the process is kept as straightforward as possible. The project consists of three separate Python scripts that should be run in succession:

1. data_prep.py: does the preparation of the data
2. train.py: trains the classification algorithm
3. test.py: tests the classification algorithm and prints performance


## The dataset

The dataset used in this project is available from:
http://ai.stanford.edu/~amaas/data/sentiment/

After unzipping the dataset (Windows users can use [7zip](http://www.7-zip.org/download.html "7zip"), the folder structure should look like this:

```
aclImdb
  |
  +-- train
  |   |
  |   +-- pos
  |   |
  |   +-- neg
  |
  +-- test
      |
      +-- pos
      |
      +-- neg
```

Both the train and test folders have subfolders called pos and neg. Inside these pos and neg folders you will find many small text files (.txt) containing the actual reviews. During data preparation these text files will be combined into a train and test CSV file.


## The process

This project uses a three step procedure to clean the data, train the classifier and predict the sentiment. Three separate Python scripts perform these steps:

### data_prep.py

This script processes all separate review text files and combines them into a train.csv and test.csv. The format of these CSV files is as follows:

|*Label*|*Text*
|---|---|
|pos|Great movie!
|neg|Did not like this movie at all...

Headers are present on the first line of the file and the delimiter character is ';'.

Besides combining all the text files, the preparation script also does some basic cleansing. This includes converting the texts to lowercase, removing HTML tags, removing accented characters, et cetera (see script for details).

### train.py

After data preparation has completed, the train.py script picks up the train.csv file and builds a classification model. The review texts are first turned into a [Document Term Matrix (DTM)](https://en.wikipedia.org/wiki/Document-term_matrix). A DTM is a large matrix of documents and the frequencies of words contained in all of them. For the two example reviews below, the simplest form of a DTM could look like:

> PosReview#1 "Absolutely a great movie! Loved the great performance by actor X"

> NegReview#3 "Hated this movie! The plot is absolutely worthless!"


||*absolutely*|*awesome*|*great*|*hated*|*loved*|...|*worthless*|
|---|---|---|---|---|---|---|---|
PosReview#1|1|0|2|0|1|...|0|
NegReview#3|1|0|0|1|0|...|1|

Rather than using raw word frequencies, the script weights the word frequencies by their appearence across all documents. Scikit-learn allows you to easily do this using a [TF-IDF vectorizer](http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html). For more on how TF-IDF is computed, see wikipedia: https://en.wikipedia.org/wiki/Tf%E2%80%93idf.

After building the DTM, we can use a classification algorithm to perform the actual classification into positive or negative reviews. For this project I used a [Logistic Regression classifier](http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html). For more on logisitic regression, see wikipedia: https://en.wikipedia.org/wiki/Logistic_regression.

Having trained both the vectorizer and classifier, we can use them for making predictions for new reviews (i.e., reviews not in the training dataset).

### test.py

This script applies the trained vectorizer and classifier to the test dataset. The algorithms will make predictions (positive or negative) for reviews that were not in the training set. Note that the vectorizer will not take into account words it has not seen (i.e., words not in the training set), which is why having a large training set is important.

By comparing the predicted labels to the actual labels in the test dataset, we can measure the performance of our classifier. The script will print out accuracy scores and the proportion of true and false positives. Finally, it will create a result dataset with the reviews and both the predicted and actual labels.


## Getting started 

### Getting Python (on Windows)

You will need Python (tested version 3.5.1) with the scikit-learn package (tested version 0.18) installed to run this project. For Windows users, I highly recommend the Anaconda distribution of Python by Continuum. It is available here: https://www.continuum.io/downloads#windows.

To check whether Python is installed, open a command prompt (press Windows key + R and type `cmd`) and type `conda list`. If scikit-learn is not in the list, try installing it with `conda install scikit-learn`.

### Getting the scripts

You can use a git client to clone the Python scripts from this project to your local system. Alternatively, you can download a zipped version of the scripts here: https://gitlab.com/lfkoning/imdb_classification/tree/master

### Getting the data

The data can be downloaded from this page: http://ai.stanford.edu/~amaas/data/sentiment/. You should extract the aclImdb folder to the same location as the Python scripts.

### Running the scripts

Open a command prompt at the folder containing the Python scripts and the IMDB dataset (for example C:\IMDB\). Next launch the scripts with the following commands:

`C:\IMDB>python data_prep.py`

`C:\IMDB>python train.py`

`C:\IMDB>python test.py`

